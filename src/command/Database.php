<?php

declare (strict_types=1);

namespace yuntu\ThinkLibrary\command;

use think\console\{Input, Output};
use think\console\input\Argument;
use yuntu\ThinkLibrary\Command;
use yuntu\ThinkLibrary\constant\DefaultConst;
use yuntu\ThinkLibrary\service\SystemService;

/**
 * 数据库修复优化
 * @author weiss <1554783799@qq.com> 2022/4/19 20:33
 * @package yuntu\ThinkLibrary\command
 *
 * 命令行调用示例：
 * @example 修复所有数据表：php think yun:database repair
 * @example 优化所有数据表：php think yun:database optimize
 */
class Database extends Command
{
    /**
     * 指令任务配置
     */
    public function configure()
    {
        $this->setName(DefaultConst::COMMAND_DATABASE);
        $this->addArgument('action', Argument::OPTIONAL, 'repair|optimize', 'optimize');
        $this->setDescription('数据库优化和修复');
    }

    /**
     * 任务执行入口
     * @param Input $input
     * @param Output $output
     * @return void
     */
    protected function execute(Input $input, Output $output) : void
    {
        $method = $input->getArgument('action');
        if (in_array($method, ['repair', 'optimize'])) {
            $this->{"_$method"}();
        } else {
            $this->output->error('操作错误，目前允许 repair|optimize');
        }
    }

    /**
     * 修复所有数据表
     */
    protected function _repair() : void
    {
        $this->setQueueProgress('正在获取需要修复的数据表', self::PROGRESS_START);

        [$tables, $total, $count] = SystemService::instance()->getTables();
        $this->setQueueProgress("总共需要修复 $total 张数据表");

        foreach ($tables as $table) {
            $this->app->db->query("REPAIR TABLE `$table`");
            $this->setQueueMessage($total, ++$count, "修复数据表 $table");
        }

        $this->setQueueProgress("已完成对 $total 张数据表修复操作", self::PROGRESS_END);
    }

    /**
     * 优化所有数据表
     */
    protected function _optimize() : void
    {
        $this->setQueueProgress('正在获取需要优化的数据表', self::PROGRESS_START);

        [$tables, $total, $count] = SystemService::instance()->getTables();
        $this->setQueueProgress("总共需要优化 $total 张数据表");

        foreach ($tables as $table) {
            $this->app->db->query("OPTIMIZE TABLE `$table`");
            $this->setQueueMessage($total, ++$count, "优化数据表 $table");
        }

        $this->setQueueProgress("已完成对 $total 张数据表优化操作", self::PROGRESS_END);
    }
}
