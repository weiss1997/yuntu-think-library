<?php

// +----------------------------------------------------------------------
// | yuntu ThinkPHP V6.0 Development Library
// +----------------------------------------------------------------------
// | 版权所有：2022~2032 云图系统
// +----------------------------------------------------------------------
// | 官方网站: 
// +----------------------------------------------------------------------
// | 开源协议：MIT
// +----------------------------------------------------------------------
// | Gitee 仓库地址：https://gitee.com/weiss1997/yuntu-think-library.git
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace yuntu\ThinkLibrary;

use think\{App, Container};

/**
 * 自定义服务基类
 * @author weiss <1554783799@qq.com> 2022/3/30 10:22
 * @package yuntu\ThinkLibrary
 */
abstract class Service
{
    /**
     * 应用实例
     * @var App
     */
    protected $app;

    /**
     * Service constructor.
     * @param App $app
     */
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->initialize();
    }

    /**
     * 初始化服务
     */
    protected function initialize()
    {
    }

    /**
     * 静态实例对象
     * @param mixed $var 实例参数
     * @param bool $new 创建新实例
     * @return static|mixed
     */
    public static function instance($var = [], bool $new = false)
    {
        return Container::getInstance()->make(static::class, $var, $new);
    }
}
