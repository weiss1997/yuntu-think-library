<?php

// +----------------------------------------------------------------------
// | yuntu ThinkPHP V6.0 Development Library
// +----------------------------------------------------------------------
// | 版权所有：2022~2032 云图系统
// +----------------------------------------------------------------------
// | 官方网站: 
// +----------------------------------------------------------------------
// | 开源协议：MIT
// +----------------------------------------------------------------------
// | Gitee 仓库地址：https://gitee.com/weiss1997/yuntu-think-library.git
// +----------------------------------------------------------------------

namespace yuntu\ThinkLibrary\traits;

/**
 * where 查询方法扩展
 *
 * 需要手动在 vendor/topthink/think-orm/src/db/BaseQuery 数据查询基础类中引入
 * use \yuntu\ThinkLibrary\traits\WhereComplex;
 *
 * @author weiss <1554783799@qq.com> 2022/4/6 10:30
 * @package yuntu\ThinkLibrary\traits
 */
trait WhereComplex
{
    /**
     * 复合查询语句组装
     * @param array $where 查询语句
     * @param array $params 查询参数（未定义时自动获取 GET|POST 请求参数）
     * @return $this
     *
     * 参数说明：
     * 1、TP语法支持：支持索引数组语法，不支持关联数组语法
     *
     * 2、内置逻辑查询支持
     * '_multi' => 'AND',// 查询逻辑支持：OR AND XOR
     * '_multi' => ['AND'=>['id','name'],'OR'=>'phone,sex'],// 指定字段查询逻辑：支持数组 或 逗号分割模式
     *
     * 3、自动获取参数查询方式：基础语法示例
     * 'id' => '=',                         // key=字段名，value=查询表达式（like，=，>=，in）等......
     * 'id' => ['='],                       // key=字段名，array（查询表达式）
     * 'id' => ['like', '%', 'uid|sid'],    // key=字段名，array（查询表达式，附属规则，字段映射）
     * 'a.id' => ['='],                     // 别名支持：key=字段名，array（查询表达式）
     *
     * 4、字段定义说明：
     * 原始字段：直接对应获取，请求参数中的查询值
     * 别名字段：当字段存在别名时，会自动从 请求参数中过滤掉别名后，再获取对应的查询参数值
     *
     * 5、字段映射说明：
     * 从请求参数获取对应值后，key最终会替换成定义好的映射字段
     *
     * 6、附属规则说明（需要对应 “查询表达式“ 才能使用）
     * 1.模糊查询：like（% 全模糊查询、L% 左边模糊查询、R% 右边模糊查询）
     * 2.时间区间：between time（区间分隔符）; 示例：2021-04-15 11:39:31~2021-04-15 11:50:00
     *
     * @version 1.1
     */
    public function whereComplex(array $where = [], array $params = []) : self
    {
        $params = $params ?: request()->param();
        $_multi = $where['_multi'] ?? [];
        if (array_key_exists('_multi', $where)) unset($where['_multi']);

        $newWhere = [];
        foreach ($where as $k => $v) {
            // 关联数组条件重构：适配 ['name'=>'like']（表达式查询结构）；不再支持 ['name'=>'我我我']（直接赋值查询结构）
            if (is_string($k) && is_string($v)) $v = [$v];

            // 过滤非索引数组的查询结构
            if (!is_int($k) && !is_array($v) || is_int($k) && is_string($v)) continue;

            // 判断where条件是否需要自动获取参数查询
            if (is_string($k) && is_array($v)) {
                // 判断是否定义别名
                list($alias, $name) = (strpos($k, '.') ? explode('.', $k, 2) : ['', $k]);

                // 过滤空值查询
                if (!isset($params[$name]) || empty($params[$name]) && strlen($params[$name]) <= 0 || empty($v)) continue;

                // 拆分附属规则
                $count = 4 - count($v);
                if ($count > 0) {
                    $v = array_pad($v, count($v) + $count, null);
                }
                list($op, $condition, $orm, $logic) = $v;

                // 所有附属规则都在此判断内处理
                if (!empty($condition)) {
                    if ($op == 'like') {
                        $params[$name] = (in_array($condition, ['%', 'L%']) ? '%' : '') . $params[$name] . (in_array($condition, ['%', 'R%']) ? '%' : '');
                    } else if ($op == 'between time') {
                        if (count($exp = explode($condition, $params[$name])) !== 2) continue;
                        $params[$name] = [strtotime($exp[0]), strtotime($exp[1]) + (strlen($exp[1]) == 10 ? 86400 : 0)];
                    }
                }

                // 重新组装where语句
                $alias    = $alias ? $alias . '.' : '';
                $trueName = $orm ?: ($alias . $name);
                $v        = [$trueName, $op, $params[$name], $logic ?: ''];
            }

            // 查询逻辑 and or xor 判断
            $logic = empty($_multi) ? ($v[3] ?? '') : $_multi;
            if ($logic = $logic ?: 'AND') {
                foreach (is_array($logic) ? $logic : [] as $k1 => $v1) {
                    is_string($v1) && $v1 = array_map('trim', explode(',', $v1));
                    $logic = in_array($v[0], $v1) ? $k1 : 'AND';
                }
            }
            $newWhere[$logic][] = $v;
        }
        // where语句逻辑组装
        foreach ($newWhere as $key_logic => $val_array) {
            $param = $val_array;
            array_shift($param);
            $this->parseWhereExp($key_logic, $val_array, null, null, $param);
        }
        return $this;
    }
}
