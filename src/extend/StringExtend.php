<?php

// +----------------------------------------------------------------------
// | yuntu ThinkPHP V6.0 Development Library
// +----------------------------------------------------------------------
// | 版权所有：2022~2032 云图系统
// +----------------------------------------------------------------------
// | 官方网站: 
// +----------------------------------------------------------------------
// | 开源协议：MIT
// +----------------------------------------------------------------------
// | Gitee 仓库地址：https://gitee.com/weiss1997/yuntu-think-library.git
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace yuntu\ThinkLibrary\extend;

/**
 * 字符串处理扩展
 * @author weiss <1554783799@qq.com> 2022/4/1 11:33
 * @package yuntu\ThinkLibrary\extend
 */
class StringExtend
{
    /**
     * 获取字符串中的前几个汉字
     * @param string $str 被截取的字符串
     * @param int $length 需要截取前几个汉字
     * @return string
     */
    public static function gain_chinese(string $str = '', int $length = 2) : string
    {
        $count     = 0;
        $result    = '';
        $str_count = strlen($str);// 字符个数
        if ($str_count > 0) {
            for ($i = 0; $i < $str_count; $i++) {
                $str_i = mb_substr($str, $i, 1, 'utf-8');// 获取字符串中的第$i个字符
                if (preg_match('/^[\x{4e00}-\x{9fa5}]+$/u', $str_i) && $count < $length) {
                    $result .= $str_i;
                    $count++;
                }
            }
        }
        return $result;
    }

    /**
     * 字符串首字母获取
     * @param string $str 需要提取首字母的字符串
     * @return string|null
     */
    public static function get_first_charter(string $str = '') : ?string
    {
        $letter = '';
        if (empty($str)) return $letter;
        $fchar = ord($str[0]);
        if ($fchar >= ord('A') && $fchar <= ord('z')) {
            return strtoupper($str[0]);
        }
        $s1  = iconv('UTF-8', 'gb2312', $str);
        $s2  = iconv('gb2312', 'UTF-8', $s1);
        $s   = $s2 == $str ? $s1 : $str;
        $asc = ord($s[0]) * 256 + ord($s[1]) - 65536;

        $ascArray = [
            'A' => [-20319, -20284],
            'B' => [-20283, -19776],
            'C' => [-19775, -19219],
            'D' => [-19218, -18711],
            'E' => [-18710, -18527],
            'F' => [-18526, -18240],
            'G' => [-18239, -17923],
            'H' => [-17922, -17418],
            'J' => [-17417, -16475],
            'K' => [-16474, -16213],
            'L' => [-16212, -15641],
            'M' => [-15640, -15166],
            'N' => [-15165, -14923],
            'O' => [-14922, -14915],
            'P' => [-14914, -14631],
            'Q' => [-14630, -14150],
            'R' => [-14149, -14091],
            'S' => [-14090, -13319],
            'T' => [-13318, -12839],
            'W' => [-12838, -12557],
            'X' => [-12556, -11848],
            'Y' => [-11847, -11056],
            'Z' => [-11055, -10247],
        ];

        foreach ($ascArray as $key => $val) {
            if ($asc >= $val[0] && $asc <= $val[1]) {
                $letter = $key;
                break;
            }
        }
        return $letter;
    }

    /**
     * 字符串 * 号替换
     * @param string $str 要替换的字符串
     * @param int $startLen 开始长度
     * @param int $endLen 结束长度
     * @return string
     */
    public static function string_replace(string $str = '', int $startLen = 4, int $endLen = 4) : string
    {
        if (strlen($str) < ($startLen + $endLen + 1)) return $str;
        $count  = strlen($str) - $startLen - $endLen;
        $repeat = str_repeat('*', $count);
        return preg_replace('/(\d{' . $startLen . '})\d+(\d{' . $endLen . '})/', '${1}' . $repeat . '${2}', $str);
    }

    /**
     * 字符串掩码
     * @param string $str 要替换的字符串
     * @param int $first 开始长度
     * @param int $last 结束长度
     * @return string
     */
    public static function string_mask(string $str = '', int $first = 2, int $last = 1) : string
    {
        if (empty($str)) return '';

        $len    = strlen($str);
        $toShow = $first + $last;

        $value = substr($str, 0, $len <= $toShow ? 0 : $first);
        $value .= str_repeat('*', $len - ($len <= $toShow ? 0 : $toShow));
        $value .= substr($str, $len - $last, $len <= $toShow ? 0 : $last);
        return $value;
    }

    /**
     * 截取姓名首字
     * @param string $name 姓名
     * @return string
     */
    public static function substr_name(string $name = '') : string
    {
        $strLen   = mb_strlen($name, 'utf-8');
        $firstStr = mb_substr($name, 0, 1, 'utf-8');
        return $firstStr . str_pad('', $strLen - 1, '*', STR_PAD_LEFT);
    }

    /**
     * emoji表情符过滤
     * @param string $str 需要过滤的字符
     * @return string
     */
    public static function filter_emoji(string $str = '') : string
    {
        return preg_replace_callback('/./u',
            function(array $match){
                return strlen($match[0]) >= 4 ? '' : $match[0];
            }, $str);
    }

    /**
     * 字符分割数组（含中文）
     * @param string $str 需要分割的字符
     * @return mixed|string
     */
    public static function mb_str_split(string $str = '')
    {
        if (empty($str)) return '';
        preg_match_all("/./u", $str, $arr);
        return $arr[0];
    }

    /**
     * 过滤括号内字符串
     * @param string $str 字符串
     * @param string $start 起始位
     * @param string $end 结束位
     * @return string|string[]|null
     */
    public static function filterInBracketsStr(string $str = '', string $start = '（', string $end = '）')
    {
        return preg_replace("#$start*[\s\S]*?$end#", '', $str);
    }

    /**
     * 长连接生成短链接
     *
     * 算法描述：
     * 1、使用6个字符来表示短链接，我们使用ASCII字符中的'a'-'z','0'-'9','A'-'Z'，共计62个字符做为集合
     * 2、每个字符有62种状态，六个字符就可以表示62^6（56800235584），那么如何得到这六个字符
     * 3、对传入的长URL+设置key值 进行Md5，得到一个32位的字符串(32 字符十六进制数)，即16的32次方
     * 3、将这32位分成四份，每一份8个字符，将其视作16进制串与0x3fffffff(30位1)与操作, 即超过30位的忽略处理
     * 3、这30位分成6段, 每5个一组，算出其整数值，然后映射到我们准备的62个字符中, 依次进行获得一个6位的短链接地址
     *
     * @param string $domain 域名
     * @param string $longUrl 长链接（不含域名）
     * @param bool $getOne 是否只获取一个短链
     * @param string $key 自定义密钥
     * @return array|string
     */
    public static function shortUrl(string $domain, string $longUrl, bool $getOne = true, string $key = 'yunshan')
    {
        $base32  = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $urlHash = md5($key . $longUrl);
        $len     = strlen($urlHash);
        // 将加密后的串分成4段，每段4字节，对每段进行计算，一共可以生成四组短连接
        $short_url_list = [];
        for ($i = 0; $i < 4; $i++) {
            $urlHashPiece = substr($urlHash, $i * $len / 4, $len / 4);
            // 将分段的位与0x3fffffff做位与，0x3fffffff表示二进制数的30个1，即30位以后的加密串都归零
            $hex       = hexdec($urlHashPiece) & 0x3fffffff;// 此处需要用到 hexdec() 将16进制字符串转为10进制数值型，否则运算会不正常
            $short_url = $domain;
            // 生成6位短连接
            for ($j = 0; $j < 6; $j++) {
                // 将得到的值与0x0000003d,3d为61，即charset的坐标最大值
                $short_url .= $base32[$hex & 0x0000003d];
                // 循环完以后将hex右移5位
                $hex = $hex >> 5;
            }
            $short_url_list[] = $short_url;
        }
        return $getOne ? $short_url_list[0] : $short_url_list;
    }
}
