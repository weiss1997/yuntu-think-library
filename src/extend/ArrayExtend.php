<?php

// +----------------------------------------------------------------------
// | yuntu ThinkPHP V6.0 Development Library
// +----------------------------------------------------------------------
// | 版权所有：2022~2032 云图系统
// +----------------------------------------------------------------------
// | 官方网站: 
// +----------------------------------------------------------------------
// | 开源协议：MIT
// +----------------------------------------------------------------------
// | Gitee 仓库地址：https://gitee.com/weiss1997/yuntu-think-library.git
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace yuntu\ThinkLibrary\extend;

/**
 * 数组处理扩展
 * @author weiss <1554783799@qq.com> 2022/4/1 11:33
 * @package yuntu\ThinkLibrary\extend
 */
class ArrayExtend
{
    /**
     * 将返回的数据集转换成树
     * @param array $list 数据集
     * @param integer $root 根节点ID
     * @param string $pk 主键
     * @param string $pid 父节点名称
     * @param string $child 子节点名称
     * @return array
     */
    public static function list_to_tree(array $list = [], int $root = 0, string $pk = 'id', string $pid = 'pid', string $child = '_child') : array
    {
        $tree  = [];// 创建Tree
        $refer = [];// 创建基于主键的数组引用
        foreach ($list as $key => $data) $refer[$data[$pk]] =& $list[$key];
        foreach ($list as $key => $data) {
            // 判断是否存在parent
            $parentId = $data[$pid];
            if ($root == $parentId) {
                $tree[$data[$pk]] =& $list[$key];
            } else {
                if (isset($refer[$parentId])) {
                    $parent           =& $refer[$parentId];
                    $parent[$child][] =& $list[$key];
                }
            }
        }
        return $tree;
    }

    /**
     * 多维数组转一维数组
     * @param array $data 多维数组
     * @return array
     */
    public static function multi_to_array(array $data = []) : array
    {
        $data = array_filter($data);
        if (empty($data)) return [];
        return array_reduce($data, function($result, $value){
            return array_merge($result, array_values($value));
        }, []);
    }

    /**
     * 数组追加指的参数
     * @param array $data 被追加的数据
     * @param array $append 需要追加的数据
     * @return array
     */
    public static function array_append(array $data, array $append) : array
    {
        array_walk($data, function(&$v, $k, $p){
            $v = array_merge($v, $p);
        }, $append);
        return $data;
    }

    /**
     * 递归返回树状列表
     * @param array $data 数据列表
     * @param int $pid 父级ID
     * @param int $level 分类层级
     * @return array
     */
    public static function get_tree(array $data, int $pid = 0, int $level = 0) : array
    {
        static $_ret = [];
        foreach ($data as $k => $v) {
            if ($v['pid'] == $pid) {
                $v['level'] = $level + 1;
                $_ret[]     = $v;
                unset($data[$k]);
                self::get_tree($data, $v['id'], $v['level']);
            }
        }
        return $_ret;
    }

    /**
     * 获取数据树子ID
     * @param array $list 数据列表
     * @param int $id 起始ID
     * @param string $key 子Key
     * @param string $pkey 父Key
     * @return array
     */
    public static function get_arr_sub_ids(array $list, int $id = 0, string $key = 'id', string $pkey = 'pid') : array
    {
        if (empty($list) || $id == 0) {
            return [];
        }
        $ids = [$id];
        foreach ($list as $vo) {
            if (intval($vo[$pkey]) > 0 && intval($vo[$pkey]) === $id) {
                $ids = array_merge($ids, self::get_arr_sub_ids($list, intval($vo[$key]), $key, $pkey));
            }
        }
        return $ids;
    }

    /**
     * 删除空字段
     * @param array $list 数据列表
     * @return array
     */
    public static function removeEmptyField(array $list) : array
    {
        return array_map(function($v){
            return array_filter($v);
        }, $list);
    }
}
