<?php

// +----------------------------------------------------------------------
// | yuntu ThinkPHP V6.0 Development Library
// +----------------------------------------------------------------------
// | 版权所有：2022~2032 云图系统
// +----------------------------------------------------------------------
// | 官方网站: 
// +----------------------------------------------------------------------
// | 开源协议：MIT
// +----------------------------------------------------------------------
// | Gitee 仓库地址：https://gitee.com/weiss1997/yuntu-think-library.git
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace yuntu\ThinkLibrary\extend;

/**
 * 解析PHPDoc注释
 * @link https://github.com/murraypicton/Doqumentor
 * @author weiss <1554783799@qq.com> 2022/4/1 10:22
 * @package app\utils
 */
class DocParser
{
    /**
     * 解析内容
     * @var array
     */
    private $params = [];

    /**
     * 解析注释
     * @param string $doc 注释文本内容
     * @return array
     */
    public static function parse(string $doc = '') : array
    {
        $mode = new static();
        if ($doc == '') return $mode->params;
        // 获取内容
        if (preg_match('#^/\*\*(.*)\*/#s', $doc, $comment) === false) return $mode->params;
        $comment = trim($comment[1]);
        // 获取所有行并从第一个字符中去掉 *
        if (preg_match_all('#^\s*\*(.*)#m', $comment, $lines) === false) return $mode->params;
        $mode->parseLines($lines[1]);
        return $mode->params;
    }

    /**
     * 注释体
     * @param array $lines 内容
     * @return void
     */
    private function parseLines(array $lines) : void
    {
        $desc = [];
        foreach ($lines as $line) {
            $parsedLine = $this->parseLine($line);// 解析行
            if ($parsedLine === false && !isset($this->params['description'])) {
                if (isset($desc)) {
                    $this->params['description'] = implode(PHP_EOL, $desc);// 将第一行存储在简短描述中
                }
                $desc = [];
            } else if ($parsedLine !== false) {
                $desc[] = $parsedLine;// 将行存储在长描述中
            }
        }
        $desc = implode(' ', $desc);
        if (!empty($desc)) $this->params['long_description'] = $desc;
    }

    /**
     * 解析行
     * @param string $line 注释行内容
     * @return false|string
     */
    private function parseLine(string $line)
    {
        $line = trim($line);// 从行中修剪空白
        if (empty($line)) return false;// 空行
        if (strpos($line, '@') === 0) {
            if (strpos($line, ' ') > 0) {
                // 获取参数名称
                $param = substr($line, 1, strpos($line, ' ') - 1);
                // 获取参数值
                $value = substr($line, strlen($param) + 2);
            } else {
                $param = substr($line, 1);
                $value = '';
            }
            // 解析该行
            if ($this->setParam($param, $value)) return false;
        }
        return $line;
    }

    /**
     * 设置参数
     * @param string $param 参数名
     * @param string $value 参数值
     * @return bool
     */
    private function setParam(string $param, string $value) : bool
    {
        if ($param == 'param' || $param == 'return')
            $value = $this->formatParamOrReturn($value);
        if ($param == 'class')
            list($param, $value) = $this->formatClass($value);
        if (empty($this->params[$param])) {
            $this->params[$param] = $value;
        } else if ($param == 'param') {
            $arr                  = array(
                $this->params[$param],
                $value
            );
            $this->params[$param] = $arr;
        } else {
            $this->params[$param] = $value + $this->params[$param];
        }
        return true;
    }

    /**
     * 格式类
     * @param string $value 参数值
     * @return array
     */
    private function formatClass(string $value) : array
    {
        $r = preg_split("[[()]]", $value);
        if (is_array($r)) {
            $param = $r[0];
            parse_str($r[1], $value);
            foreach ((array)$value as $key => $val) {
                $val = explode(',', $val);
                if (count($val) > 1) $value[$key] = $val;
            }
        } else {
            $param = 'Unknown';
        }
        return [$param, $value];
    }

    /**
     * 格式参数或返回
     * @param string $string 参数内容
     * @return string
     */
    private function formatParamOrReturn(string $string) : string
    {
        if ($pos = strpos($string, ' ')) {
            $type = substr($string, 0, $pos);
            return '(' . $type . ')' . substr($string, $pos + 1);
        }
        return $string;
    }
}
