<?php

// +----------------------------------------------------------------------
// | yuntu ThinkPHP V6.0 Development Library
// +----------------------------------------------------------------------
// | 版权所有：2022~2032 云图系统
// +----------------------------------------------------------------------
// | 官方网站: 
// +----------------------------------------------------------------------
// | 开源协议：MIT
// +----------------------------------------------------------------------
// | Gitee 仓库地址：https://gitee.com/weiss1997/yuntu-think-library.git
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace yuntu\ThinkLibrary\extend;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\{ClientException, ConnectException, GuzzleException, ServerException, TransferException};

/**
 * CURL模拟请求扩展
 * @author weiss <1554783799@qq.com> 2022/3/11 11:48
 * @package yuntu\ThinkLibrary\extend
 */
class HttpExtend
{
    /**
     * HTTP回调状态码
     * @var int
     */
    public $httpCode;

    /**
     * HTTP回调消息
     * @var string
     */
    public $httpMessage;

    /**
     * Header请求参数
     * @var array
     */
    public $headers = [];

    /**
     * Cookie携带数据
     * @var object|array
     */
    public $cookies = [];

    /**
     * Client 配置参数
     * @var array
     */
    public $config = [];

    /**
     * @param array $config Client 配置参数
     * @psalm-var string version [请求要使用到的协议版本] [默认1.1]
     * @psalm-var bool http_errors [false 禁用HTTP协议抛出的异常(如 4xx 和 5xx 响应)，true HTTP协议出错时会抛出异常] [默认true]
     * @psalm-var bool allow_redirects [false 禁止重定向，true 来启用默认最大次数为5的重定向] [默认true]
     * @psalm-var int timeout [请求超时的秒数。使用 0 无限期的等待。如果服务器在 N 秒内没有返回响应，则超时] [默认0]
     * @psalm-var int connect_timeout [等待服务器响应超时的最大值，使用 0 将无限等待。如果客户端无法在 N 秒内连接到服务器，则超时] [默认0]
     * @psalm-var string base_uri [基URI用来合并到相关URI，可以是一个字符串或者UriInterface的实例]
     * @psalm-var bool DataVerify [请求时验证SSL证书行为] [默认true]
     */
    public function __construct(array $config = [])
    {
        $this->config = $config;
    }

    /**
     * 设置Header请求参数
     * @param array $params 参数
     * @return $this
     */
    public function setHeaders(array $params = []) : self
    {
        $this->headers = $params;
        return $this;
    }

    /**
     * 设置Cookies携带参数
     * @param mixed $params 参数
     * @return $this
     */
    public function setCookies($params = []) : self
    {
        $this->cookies = $params;
        return $this;
    }

    /**
     * 以 GET 模拟网络请求
     * @param string $url 请求地址
     * @param array $params 请求参数
     * @return false|mixed
     * @throws GuzzleException
     */
    public function get(string $url, array $params = [])
    {
        return $this->syncRequest('GET', $url, $params);
    }

    /**
     * 以 POST 模拟网络请求
     * @param string $url 请求地址
     * @param array $params 请求参数
     * @return false|mixed
     * @throws GuzzleException
     */
    public function post(string $url, array $params = [])
    {
        return $this->syncRequest('POST', $url, $params);
    }

    /**
     * 同步请求
     * @param string $method 请求方式（GET DELETE HEAD OPTIONS PATCH POST PUT）
     * @param string $url 请求地址
     * @param array $params 请求参数
     * @param bool $json 是否转json格式数据（json格式默认post请求）
     * @return false|mixed
     * @throws GuzzleException
     */
    public function syncRequest(string $method, string $url, array $params = [], bool $json = false)
    {
        try {
            $client = new Client($this->config);
            if ($params) $options[$method == 'POST' ? ($json ? 'json' : 'form_params') : 'query'] = $params;
            $options['headers'] = $this->headers;
            $options['cookies'] = $this->cookies;

            $request = $client->request($method, $url, $options);
            if (is_object($request) && $request->getStatusCode() == 200 && $request->getReasonPhrase() == 'OK') {
                $this->httpCode = 200;
                return json_decode((string)$request->getBody(), true);
            }
        } catch (TransferException $e) {
            if ($e instanceof ClientException) {// http_errors 请求参数设置成true，在400级别的错误的时候将会抛出
                $this->httpCode = 400;
            } else if ($e instanceof ServerException) {// http_errors 请求参数设置成true，在500级别的错误的时候将会抛出
                $this->httpCode = 500;
            } else if ($e instanceof ConnectException) {// 在发送网络错误（连接超时、DNS错误等）抛出异常
                $this->httpCode = 408;
            }
            $this->httpMessage = $e->getMessage();
        }
        return false;
    }
}
