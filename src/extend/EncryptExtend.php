<?php

// +----------------------------------------------------------------------
// | yuntu ThinkPHP V6.0 Development Library
// +----------------------------------------------------------------------
// | 版权所有：2022~2032 云图系统
// +----------------------------------------------------------------------
// | 官方网站: 
// +----------------------------------------------------------------------
// | 开源协议：MIT
// +----------------------------------------------------------------------
// | Gitee 仓库地址：https://gitee.com/weiss1997/yuntu-think-library.git
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace yuntu\ThinkLibrary\extend;

/**
 * PHP加解密：适用于php7.2以上、可与java互解
 * @author weiss <1554783799@qq.com> 2022/3/9 16:39
 * @package yuntu\ThinkLibrary\extend
 */
class EncryptExtend
{
    /**
     * AES-ECB 加密
     * @param string $string 字符串
     * @param string $key 密钥
     * @return string
     */
    public static function encryptAesEcb(string $string, string $key) : string
    {
        $key = substr(openssl_digest(openssl_digest($key, 'sha1', true), 'sha1', true), 0, 16);
        // openssl_encrypt 加密不同Mcrypt，对秘钥长度要求，超出16加密结果不变
        $data = openssl_encrypt($string, 'AES-128-ECB', $key, OPENSSL_RAW_DATA);
        return base64_encode($data);
    }

    /**
     * AES-ECB 解密
     * @param string $string 字符串
     * @param string $key 密钥
     * @return false|string
     */
    public static function decryptAesEcb(string $string, string $key)
    {
        $key       = substr(openssl_digest(openssl_digest($key, 'sha1', true), 'sha1', true), 0, 16);
        $encrypted = base64_decode($string);
        return openssl_decrypt($encrypted, 'aes-128-ecb', $key, OPENSSL_RAW_DATA);
    }

    /**
     * DES-CBC 加密
     * @param string $string 字符串
     * @param string $secretKey 密钥
     * @param mixed $iv 向量
     * @return string
     */
    public static function encryptDesCbc(string $string, string $secretKey, $iv = "\x01\x02\x03\x04\x05\x06\x07\x08") : string
    {
        return base64_encode(openssl_encrypt($string, 'des-cbc', $secretKey, OPENSSL_RAW_DATA, $iv));
    }

    /**
     * DES-CBC 解密
     * @param string $string 字符串
     * @param string $secretKey 密钥
     * @param mixed $iv 向量
     * @return false|string
     */
    public static function decryptDesCbc(string $string, string $secretKey, $iv = "\x01\x02\x03\x04\x05\x06\x07\x08")
    {
        return openssl_decrypt(base64_decode($string), 'des-cbc', $secretKey, OPENSSL_RAW_DATA, $iv);
    }

    /**
     * PKCS7Padding / PKCS5Padding 填充；（PKCS5Padding 填充为8字节）
     * @param string $data 需要填充的字符
     * @param int $blockhouse 块大小
     * @return string
     */
    public static function pad(string $data, int $blockhouse = 16) : string
    {
        $pad = $blockhouse - (strlen($data) % $blockhouse);
        return $data . str_repeat(chr($pad), $pad);
    }

    /**
     * PKCS7Padding / PKCS5Padding 填充删除
     * @param string $text 需要移出填充的字符
     * @return false|string
     */
    public static function unpaid(string $text)
    {
        $pad = ord($text[strlen($text) - 1]);
        if ($pad > strlen($text)) {
            return false;
        }
        if (strspn($text, chr($pad), strlen($text) - $pad) != $pad) {
            return false;
        }
        return substr($text, 0, -1 * $pad);
    }

    /**
     * ZeroPadding 填充
     * @param string $data 需要填充的字符
     * @param int $blockhouse 块大小
     * @return string
     */
    public static function padZero(string $data, int $blockhouse = 16) : string
    {
        $pad = $blockhouse - (strlen($data) % $blockhouse);
        return $data . str_repeat("\0", $pad);
    }

    /**
     * ZeroPadding 填充删除
     * @param string $data 需要移出填充的字符
     * @return string
     */
    public static function unpaidZero(string $data) : string
    {
        return rtrim($data, "\0");
    }

    /**
     * Base64url 安全编码
     * @param string $text 待加密文本
     * @return string
     */
    public static function enSafe64(string $text) : string
    {
        return rtrim(strtr(base64_encode($text), '+/', '-_'), '=');
    }

    /**
     * Base64url 安全解码
     * @param string $text 待解密文本
     * @return string
     */
    public static function deSafe64(string $text) : string
    {
        return base64_decode(str_pad(strtr($text, '-_', '+/'), strlen($text) % 4, '='));
    }

    /**
     * MD5加盐
     * @param string $str 需要加密的字符
     * @param string $key 密钥
     * @return string
     */
    public static function md5(string $str, string $key) : string
    {
        return md5(sha1($str) . $key);
    }
}
